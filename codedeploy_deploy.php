<?php

require 'vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\CodeDeploy\CodeDeployClient;

if (count($argv) != 2) {
    echo('Usage: ' . $argv[0] . ' <file to deploy>');
    exit(1);
}

$archive = $argv[1];
if (!is_file($archive)) {
    echo('Failed to locate archive: ' . $archive);
    exit(1);
}

$version_label = gmstrftime("%Y%m%d%H%M%S");
$bucket_key = getenv('APPLICATION_NAME') . '/' . $version_label . '-bitbucket_builds.zip';

// Create S3 object
try {
    $s3 = S3Client::factory();
} catch (Exception $e) {
    echo("Failed to create S3 client.\n" . $e->getMessage() . "\n");
    exit(1);
}

// Upload file to S3
try {
    $result = $s3->putObject([
        'Bucket' => getenv('S3_BUCKET'),
        'Key' => $bucket_key,
        'SourceFile' => $argv[1]
    ]);
} catch (Exception $e) {
    echo("Failed to upload artifact to S3.\n" . $e->getMessage() . "\n");
    exit(1);
}

// Create CodeDeploy
try {
    $cd = CodeDeployClient::factory();
} catch (Exception $e) {
    echo("Failed to create CodeDeploy client.\n" . $e->getMessage() . "\n");
    exit(1);
}

// Deploy new code
try {
    $deployment = $cd->createDeployment(array(
        'applicationName' => getenv('APPLICATION_NAME'),
        'deploymentGroupName' => getenv('DEPLOYMENT_GROUP_NAME'),
        'revision' => array(
            'revisionType' => 'S3',
            's3Location' => array(
                'bucket' => getenv('S3_BUCKET'),
                'key' => $bucket_key,
                'bundleType' => 'zip',
            ),
        ),
        'deploymentConfigName' => getenv('DEPLOYMENT_CONFIG'),
        'description' => 'New deployment from BitBucket',
        'ignoreApplicationStopFailures' => true,
    ));
} catch (Exception $e) {
    echo("Failed to deploy application revision.\n" . $e->getMessage() . "\n");
    exit(1);
}

// Wait for deployment to complete
while (true) {
    try {
        $response = $cd->getDeployment(array('deploymentId' => $deployment->deploymentId));
        $deploymentStatus = $response->deploymentInfo['status'];
        if ($deploymentStatus == 'Succeeded') {
            print ("Deployment Succeeded");
            break;
        } elseif ($deploymentStatus == 'Failed' || $deploymentStatus == 'Stopped') {
            print ("Deployment Failed");
            exit(1);
        } elseif ($deploymentStatus == 'InProgress' || $deploymentStatus == 'Queued' || $deploymentStatus == 'Created') {
            continue;
        }
    } catch (Exception $e) {
        echo("Failed to deploy application revision.\n" . $e->getMessage() . "\n");
        exit(1);
    }
}

// Done!
exit(0);
