<?php

// This file requires the zip extension to be enabled
// Class by <umbalaconmeogia at NOSPAM dot gmail dot com> from http://php.net/manual/en/class.ziparchive.php

class HZip
{
    /**
     * Add files and sub-directories in a folder to zip file.
     * @param string $folder
     * @param ZipArchive $zipFile
     * @param int $exclusiveLength Number of text to be exclusived from the file path.
     */
    private static function folderToZip($folder, &$zipFile, $exclusiveLength)
    {
        $handle = opendir($folder);
        while (false !== $f = readdir($handle)) {
            if ($f != '.' && $f != '..' && $f[0] != '.') {
                $filePath = "$folder/$f";
                // Remove prefix from file path before add to zip.
                $localPath = substr($filePath, $exclusiveLength);
                if (is_file($filePath)) {
                    $zipFile->addFile($filePath, $localPath);
                } elseif (is_dir($filePath)) {
                    // Add sub-directory.
                    $zipFile->addEmptyDir($localPath);
                    self::folderToZip($filePath, $zipFile, $exclusiveLength);
                }
            }
        }
        closedir($handle);
    }

    /**
     * Zip a folder (include itself).
     * Usage:
     *   HZip::zipDir('/path/to/sourceDir', '/path/to/out.zip');
     *
     * @param string $sourcePath Path of directory to be zip.
     * @param string $outZipPath Path of output zip file.
     */
    public static function zipDir($sourcePath, $outZipPath)
    {
        $pathInfo = pathInfo($sourcePath);
        $parentPath = $pathInfo['dirname'];
        $dirName = $pathInfo['basename'];
        $normalizedPath = $pathInfo['dirname'] . '/' . $pathInfo['basename'];
        if ($sourcePath == '.') {
            $normalizedPath = '.';
        }

        $z = new ZipArchive();
        $z->open($outZipPath, ZIPARCHIVE::CREATE);
        self::folderToZip($normalizedPath, $z, strlen("$parentPath/"));
        $z->close();
    }
}

// Read arguments and pack zip

if (count($argv) !== 3) {
    echo('usage: ' . $argv[0] . ' <output path> <path to compress>');
    exit(1);
}

$archive_path = $argv[1];
$path_to_pack = $argv[2];

if (!is_dir($path_to_pack)) {
    echo('Could not read ' . $path_to_pack);
    exit(1);
}

HZip::zipDir($path_to_pack, $archive_path);
